DEPLOY_CONF_NAME:aws-ec2-arm64 = "AWS EC2 ARM64"

# To create Amazon Machine Image (AMI)
INHERIT += "aws-ec2-image"

# Use EGLFS graphics backend for Qt by default, and use
# profile script to switch to vnc if GPU not available
QT_QPA_PLATFORM = "eglfs"

# Replace connman with NetworkManager for cloud-init
IMAGE_INSTALL:append = " networkmanager"
RDEPENDS:packagegroup-b2qt-embedded-base:remove = "connman"
RDEPENDS:qtdeviceutilities:remove = "connman"
RDEPENDS:packagegroup-b2qt-embedded-tools:remove = "connman-client"

# Nvidia GPU drivers instead of Nouveau, as explained in
# NVIDIA Accelerated Linux Graphics Driver README and Installation Guide
NVIDIA_DEVNET_MIRROR ?= "${BSPDIR}/sources/nvidia-devnet-mirror"
MACHINEOVERRIDES .= "${@':nvidia-gpu' if os.path.exists(d.getVar('NVIDIA_DEVNET_MIRROR')) else ''}"
IMAGE_INSTALL:append:nvidia-gpu = ' nvidia nvidia-firmware nvidia-share'
KERNEL_MODULE_PROBECONF:append:nvidia-gpu = " nvidia"
module_conf_nvidia:nvidia-gpu = "options nvidia-drm modeset=1"
KERNEL_MODULE_AUTOLOAD:append:nvidia-gpu = " nvidia.ko nvidia-modeset.ko nvidia-drm.ko"

# Disable nouveau kernel module
ROOTFS_POSTPROCESS_COMMAND:append:nvidia-gpu = "nouveau_disable;"
nouveau_disable() {
  mkdir -p ${IMAGE_ROOTFS}${sysconfdir}/modprobe.d
  printf "blacklist nouveau\noptions nouveau modeset=0\n" > \
    ${IMAGE_ROOTFS}${sysconfdir}/modprobe.d/nouveau.conf
}

DEPLOY_CONF_IMAGE_TYPE = "wic.vhd"

QBSP_IMAGE_CONTENT += " \
    ${IMAGE_LINK_NAME}.${DEPLOY_CONF_IMAGE_TYPE} \
    ${IMAGE_LINK_NAME}.conf \
    ${IMAGE_LINK_NAME}.info \
    ${IMAGE_LINK_NAME}.testdata.json \
    "
# Kernel version compatible with nvidia drivers
PREFERRED_VERSION_linux-yocto = "6.6%"
PREFERRED_VERSION_linux-yocto-rt = "${PREFERRED_VERSION_linux-yocto}"
