do_install:append() {
    install -d ${D}${nonarch_base_libdir}/firmware
    install -m 0444 -D ${S}/firmware/gsp_tu10x.bin ${D}${nonarch_base_libdir}/firmware/nvidia/${PV}/gsp_tu10x.bin
}

FILES:${PN}-firmware = " \
    ${nonarch_base_libdir}/firmware \
"

PACKAGES += "${PN}-firmware"
INSANE_SKIP:${PN}-firmware = "arch"
