do_install:append() {
    install -d ${D}${libdir}/nvidia
    cp ${S}/libEGL* ${D}${libdir}/nvidia/
    ln -sf libEGL.so.${PV} ${D}${libdir}/nvidia/libEGL.so.1
    ln -sf libEGL_nvidia.so.${PV} ${D}${libdir}/nvidia/libEGL_nvidia.so.0

    cp ${S}/libGL* ${D}${libdir}/nvidia/
    ln -sf libGL.so.1.7.0 ${D}${libdir}/nvidia/libGL.so.1
    ln -sf libGLESv1_CM.so.1.2.0 ${D}${libdir}/nvidia/libGLESv1_CM.so.1
    ln -sf libGLESv2.so.2.1.0 ${D}${libdir}/nvidia/libGLESv2.so.2

    cp ${S}/libnvidia* ${D}${libdir}/nvidia/
    ln -sf libnvidia-egl-gbm.so.1.1.0 ${D}${libdir}/nvidia/libnvidia-egl-gbm.so.1
    ln -sf libnvidia-egl-wayland.so.1.1.11 ${D}${libdir}/nvidia/libnvidia-egl-wayland.so.1
    ln -sf libnvidia-ml.so.${PV} ${D}${libdir}/nvidia/libnvidia-ml.so.1

    cp ${S}/libOpen* ${D}${libdir}/nvidia/

    # nvidia utils
    install -d ${D}${bindir}
    install -m 755 -D ${S}/nvidia-debugdump ${D}${bindir}/nvidia-debugdump
    install -m 755 -D ${S}/nvidia-smi ${D}${bindir}/nvidia-smi
    install -m 755 -D ${S}/nvidia-modprobe ${D}${bindir}/nvidia-modprobe
    install -m 755 -D ${S}/nvidia-cuda-mps-server ${D}${bindir}/nvidia-cuda-mps-server
    install -m 755 -D ${S}/nvidia-cuda-mps-control ${D}${bindir}/nvidia-cuda-mps-control
    install -m 755 -D ${S}/nvidia-persistenced ${D}${bindir}/nvidia-persistenced

    # nvidia-share
    install -d ${D}${datadir}/egl/egl_external_platform.d/
    install -m 0444 -D ${S}/10_nvidia.json ${D}${datadir}/egl/egl_external_platform.d/10_nvidia.json
    install -m 0444 -D ${S}/10_nvidia_wayland.json ${D}${datadir}/egl/egl_external_platform.d/10_nvidia_wayland.json
    install -m 0444 -D ${S}/15_nvidia_gbm.json ${D}${datadir}/egl/egl_external_platform.d/15_nvidia_gbm.json
}

FILES:${PN} += " \
    ${libdir}/nvidia \
    ${bindir} \
"

FILES:${PN}-doc += " \
    ${mandir} \
    ${datadir}/doc/nvidia \
"

FILES:${PN}-share = " \
    ${datadir}/egl/egl_external_platform.d \
"
PACKAGES += "${PN}-share"

INSANE_SKIP:${PN}:append = "ldflags already-stripped"
