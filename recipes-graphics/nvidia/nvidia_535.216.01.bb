COMPATIBLE_MACHINE = "nvidia-gpu"

SUMMARY = "NVIDIA GPU proprietary driver"
LICENSE = "NVIDIA-Driver-License"
LIC_FILES_CHKSUM = "file://LICENSE;md5=01c5e23f445259a6d1b4867efec45d22"

NVIDIA_ARCHIVE = "NVIDIA-Linux-${TARGET_ARCH}-${PV}"

FILESEXTRAPATHS:prepend := "${NVIDIA_DEVNET_MIRROR}:"

SRC_URI:append = " \
    file://${NVIDIA_ARCHIVE}.run \
    file://0001-Disable-GCC-warnings-to-inhibit-build-failures.patch;patchdir=${B} \
"

S = "${WORKDIR}/${NVIDIA_ARCHIVE}"
B = "${S}/kernel"

extract_nvidia() {
    if [ ! -f "${UNPACKDIR}/${NVIDIA_ARCHIVE}.run" ]; then
        bbfatal "NVIDIA drivers missing at ${UNPACKDIR}/${NVIDIA_ARCHIVE}.run"
    fi
    chmod +x ${UNPACKDIR}/${NVIDIA_ARCHIVE}.run
    rm -rf ${WORKDIR}/${NVIDIA_ARCHIVE}
    ${UNPACKDIR}/${NVIDIA_ARCHIVE}.run --extract-only --ui=none --target ${WORKDIR}/${NVIDIA_ARCHIVE}
}
# unpack_qa is also postfunc so prepend extract_nvidia before it to dismiss non-existent directory warning
do_unpack[postfuncs] =+ "extract_nvidia"

require nvidia-kernel.inc
require nvidia-user.inc
require nvidia-firmware.inc
