Boot2Qt support for Amazon Machine Image
========================================

This layer provides additional configuration and recipe appends to support
Boot2Qt on AWS EC2.

How to use this layer
---------------------

Using this layer requires Google's repo tool to be installed, you can install
it from package manager (apt install repo) or download with:

    curl https://storage.googleapis.com/git-repo-downloads/repo -o repo
    chmod a+x repo


After installing the repo tool, run following commands to initialize the build environment.

    repo init -u git://code.qt.io/yocto/boot2qt-manifest -m aws/dev.xml <-g all>
    repo sync

    export MACHINE=aws-ec2-arm64
    . ./setup-environment.sh

NVIDIA GPU drivers
------------------

To use NVIDIA GPU in the AWS EC2 instance with this Boot2Qt AMI,
you first need to download the NVIDIA driver package from
https://www.nvidia.com/en-us/drivers/details/233154/
Then, set NVIDIA_DEVNET_MIRROR variable in your local.conf to point
to the download directory.
The currently supported driver version is 535.216.01.
