# Update cloud-init to use udhcpc since dhclient was deprecated
PV = "v23.4+git"
SRCREV = "6e04a2c5403f8b5211381c15888608d1ed36fcb3"
SRC_URI = "git://github.com/canonical/cloud-init;branch=main;protocol=https"
