#!/bin/sh

# Set QT_QPA_PLATFORM=EGLFS for "user" if NVIDIA drivers exist and GPU detected,
# otherwise use VNC
NVIDIA_PATH="/usr/lib/nvidia"
QPA_USE="eglfs"
if [ -d "$NVIDIA_PATH" ]; then
  dev=`sudo LD_LIBRARY_PATH=$NVIDIA_PATH nvidia-debugdump -l|head -n 1`; n="${dev//[^0-9]/}"
  if [ -n "${n}" ]; then
    export LD_LIBRARY_PATH=$NVIDIA_PATH
    if [ `stat -c "%a" /dev/dri/card0` -ne 666 ]; then
      sudo chmod 666 /dev/dri/card0
    fi
  else
    QPA_USE="vnc"
  fi
else
  echo 'NVIDIA GPU drivers not installed'
  QPA_USE="vnc"
fi
echo "Using QT_QPA_PLATFORM=$QPA_USE"
if [ $QPA_USE == "vnc" ]; then
  sudo sed -i 's/QT_QPA_PLATFORM=eglfs/QT_QPA_PLATFORM=vnc/g' /etc/default/qt
  sudo sed -i "/LD_LIBRARY_PATH=/d" /etc/default/qt
fi
export QT_QPA_EGLFS_KMS_CONFIG=/etc/kms.conf
export QT_QPA_PLATFORM=$QPA_USE
