FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://kms.conf"
SRC_URI += "file://nvidia-gpu.sh"

do_configure:append() {
    grep -qFx 'QT_QPA_EGLFS_KMS_CONFIG=/etc/kms.conf' ${WORKDIR}/defaults || echo 'QT_QPA_EGLFS_KMS_CONFIG=/etc/kms.conf' >> ${WORKDIR}/defaults
    grep -qFx 'LD_LIBRARY_PATH=/usr/lib/nvidia' ${WORKDIR}/defaults || echo 'LD_LIBRARY_PATH=/usr/lib/nvidia' >> ${WORKDIR}/defaults
    sed -i '/XDG_RUNTIME_DIR|WAYLAND_DISPLAY/d' ${WORKDIR}/defaults
}

do_install:append() {
    install -d ${D}${sysconfdir}
    install -m 0644 ${UNPACKDIR}/kms.conf ${D}${sysconfdir}/

    install -d ${D}${sysconfdir}/profile.d
    install -m 0644 ${UNPACKDIR}/nvidia-gpu.sh ${D}${sysconfdir}/profile.d/
}
