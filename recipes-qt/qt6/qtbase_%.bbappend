FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# This is needed only when nvidia-gpu is in use but dismissing override
# to reuse Qt configuration and build for both gpu and non-gpu cases.

SRC_URI:append = "\
    file://0001-Workaround-for-surfaceless-headless.patch \
"

EXTRA_OECMAKE:append = ' -DQT_QPA_DEFAULT_EGLFS_INTEGRATION="eglfs_kms"'
